import { Log, User, UserManager } from 'oidc-client';
import { Constants } from '../helpers/Constants';

export class AuthService {
  public userManager: UserManager;

  constructor() {
    const settings = {
      authority: Constants.stsAuthority,
      client_id: Constants.clientId,
      //client_secret: Constants.clientSecret,
      redirect_uri: `${Constants.clientRoot}signin-callback.html`,
      // silent_redirect_uri: `${Constants.clientRoot}silent-renew.html`,
      post_logout_redirect_uri: `${Constants.clientRoot}`,
      response_type: "code",
      scope: Constants.clientScope,
      // metadata:{
      //   "response_types_supported": ["code","id_token","token","code id_token","code token","token id_token","code id_token token"],
      //   "request_parameter_supported":true,
      //   "request_uri_parameter_supported":false,
      //   "introspection_endpoint":"https://dmctrl.ses-test.com:8443/IdPOAuth2/introspect/ReactOIDC",
      //   "scopes_supported":["address","phone","openid","profile","email"],
      //   "issuer":"https://dmctrl.ses-test.com:8443/IdPOAuth2/ReactOIDC",
      //   "authorization_endpoint":"https://dmctrl.ses-test.com:8443/IdPOAuth2/authorize/ReactOIDC",
      //   "userinfo_endpoint":"https://dmctrl.ses-test.com:8443/IdPOAuth2/userinfo/ReactOIDC",
      //   "claims_supported":["website","zoneinfo","address","birthdate","email_verified","gender","profile","phone_number_verified","preferred_username","given_name","middle_name","locale","picture","updated_at","name","nickname","phone_number","family_name","email"],
      //   "code_challenge_methods_supported":["plain","S256"],
      //   "jwks_uri":"https://dmctrl.ses-test.com:8443/IdPOAuth2/jwk/ReactOIDC",
      //   "subject_types_supported":["pairwise"],
      //   "id_token_signing_alg_values_supported":["HS256"],
      //   "registration_endpoint":"https://dmctrl.ses-test.com:8443/IdPOAuth2/register/ReactOIDC",
      //   "token_endpoint_auth_methods_supported":["client_secret_post","client_secret_basic","client_secret_jwt","private_key_jwt"],
      //   "token_endpoint":"https://dmctrl.ses-test.com:8443/IdPOAuth2/token/ReactOIDC"}
    };
    this.userManager = new UserManager(settings);

    Log.logger = console;
    Log.level = Log.INFO;
  }

  public getUser(): Promise<User | null> {
    return this.userManager.getUser();
  }

  public login(): Promise<void> {
    return this.userManager.signinRedirect();
  }

  public renewToken(): Promise<User> {
    return this.userManager.signinSilent();
  }

  public logout() {
    sessionStorage.clear();
    //return this.userManager.signoutRedirect();
  }

  public getUserIdtoken(){
    this.userManager.getUser().then(user => {
      console.log(user);
    })
  }
}
