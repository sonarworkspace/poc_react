export class Constants {
  //Config sign&go perso
  // public static stsAuthority = 'https://dmctrl.ses-test.com:8443/IdPOAuth2/ReactOIDC/';
  // public static clientId = 'A63E59E3A759568A';
  // public static clientSecret ='4D0CEE8EF7C33B947D9186F30F9094750FE363B0C56CD84A4EF3D2B0FDA27F02';

  //Config sign&go dev Uno
  public static stsAuthority = 'https://uneo-dev.cloud.ilex.fr:8443/IdPOAuth2/ids/';
  public static clientId = '306C52206BD41D59';
  //public static clientSecret ='F0C6F885E96C2226A93F089824E7BE98FE7D489D1039C13B724CB1BC0754AE7C';

    //Config sign&go test
    // public static stsAuthority = 'https://uneo-dev.cloud.ilex.fr:8443/IdPOAuth2/ids/';
    // public static clientId = 'B485AF058D2BFB8F';
  
  public static clientRoot = 'http://my.testdomain.fr:4200/react/';
  // public static clientScope = 'openid email';

  public static clientScope = 'openid email numeroPerson contractNumber lastName firstName radiationDate salutation';
  public static apiRoot = 'https://localhost:8443/IdPOAuth2/ReactOIDC/api';
}
