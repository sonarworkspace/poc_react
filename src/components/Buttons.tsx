import * as React from 'react';

interface IButtonsProps {
  login: () => void;
  loginRib: () => void;
  getUser:() => void;
  callApi: () => void;
  renewToken: () => void;
  logout: () => void;
  // fetchTest: () => void;
  // franceConnectSearch: () => void;
}

const Buttons: React.SFC<IButtonsProps> = props => {
  if (sessionStorage.getItem("oidc.user:https://dev-id.groupe-uneo.fr/IdPOAuth2/uneo:860E7314EEC075AC")  == null){
    return (
      <div className="row">
      <div className="col-md-12 text-center" style={{ marginTop: '30px' }}>
        <button className="btn btn-primary btn-login" style={{ margin: '10px' }} onClick={props.login}>
          Login
        </button>
      </div>
    </div>
    )
  }
  return (
    <div className="row">
      <div className="col-md-12 text-center" style={{ marginTop: '30px' }}>
        {/* <button className="btn btn-primary btn-login" style={{ margin: '10px' }} onClick={props.login}>
          Login
        </button> */}
        {/* <button className="btn btn-secondary btn-getuser" style={{ margin: '10px' }} onClick={props.getUser}>
          Get User info
        </button> */}
        {/* <button className="btn btn-warning btn-getapi" style={{ margin: '10px' }} onClick={props.callApi}>
          Call API
        </button>
        <button className="btn btn-success btn-renewtoken" style={{ margin: '10px' }} onClick={props.renewToken}>
          Renew Token
        </button> */}

        {/* <button className="btn btn-dark btn-logout" style={{ margin: '10px' }} onClick={props.logout}>
          Logout
        </button> */}
        <a href="https://uneo-dev.cloud.ilex.fr:8443/user/auth/chooseschema?schema=franceconnect&sourceURL=http://localhost:4200">
          <button className="btn btn-success btn-getuser" style={{ margin: '10px' }}> Associer compte France connect</button>
        </a>
        {/* <button className="btn btn-danger btn-getuser" style={{ margin: '10px' }} onClick={props.franceConnectSearch}>
          Dissocier compte France connect
          </button> */}
          <a href="https://uneo-dev.cloud.ilex.fr:8443/user/auth/chooseschema?schema=modifyPassword&sourceURL=http://localhost:4200&theme=uneo-authentication">
          <button className="btn btn-dark btn-logout" style={{ margin: '10px' }}>Modifier mot de passe</button>
        </a>
        {/* <a href="https://uneo-dev.cloud.ilex.fr:8443/user/auth/chooseschema?schema=OtpMailModifyPasssword&sourceURL=http://localhost:4200/rib-form.html&theme=uneo-authentication"> */}
        {/* <a href="./rib-form.html">
          <button className="btn btn-primary btn-login" style={{ margin: '10px' }}>Modifier mon RIB</button>
        </a> */}
        {/* <button className="btn btn-primary btn-login" style={{ margin: '10px' }} onClick={props.loginRib}>
          Modifier mon RIB
        </button> */}
        <a href="https://epdev.groupe-uneo.fr/user/auth/chooseschema?schema=otprib&theme=uneo-authentication&sourceURL=http://localhost:4200/rib-form.html">
          <button className="btn btn-primary btn-login" style={{ margin: '10px' }} >Modifier RIB </button>
        </a>   
		<a href="https://epdev.groupe-uneo.fr/user/auth/logout?sourceURL=http://localhost:4200">
          <button className="btn btn-dark btn-logout" style={{ margin: '10px' }} onClick={props.logout}>Logout</button>
        </a>
      </div>
    </div>
  );
};

export default Buttons;
