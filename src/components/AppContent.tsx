import * as React from 'react';

import { ToastContainer, toast } from 'react-toastify';
import { ApiService } from '../services/ApiService';
import { AuthService } from '../services/AuthService';
import { RibService } from '../services/RibService';

import AuthContent from './AuthContent';
import Buttons from './Buttons';

export default class AppContent extends React.Component<any, any> {
  public authService: AuthService;
  public ribService: RibService;
  public apiService: ApiService;
  private shouldCancel: boolean;
  public idToken : string;

  constructor(props: any) {
    super(props);

    this.authService = new AuthService();
    this.ribService = new RibService();
    this.apiService = new ApiService();
    this.state = { user: {}, api: {} };
    this.shouldCancel = false;
    this.idToken = "";
  }

  public componentDidMount() {
    this.getUser();
  }

  public login = () => {
    this.authService.login();
  };

  public loginRib =() =>{
    this.ribService.login();
  }

  public callApi = () => {
    this.apiService
      .callApi()
      .then(data => {
        this.setState({ api: data.data });
        toast.success('Api return successfully data, check in section - Api response');
      })
      .catch(error => {
        toast.error(error);
      });
  };

  public componentWillUnmount() {
    this.shouldCancel = true;
  }

  public renewToken = () => {
    this.authService
      .renewToken()
      .then(user => {
        toast.success('Token has been sucessfully renewed. :-)');
        this.getUser();
      })
      .catch(error => {
        toast.error(error);
      });
  };

  public logout = () => {
    this.authService.logout();
  };

  public getUser () {
    var userMount = false;
    this.authService.getUser().then(user => {
      if (user) {
        toast.success('User has been successfully loaded from store.');
        userMount = true
      } else {
        toast.info('You are not logged in.');
      }

      if (!this.shouldCancel) {
        this.setState({ user });
      }
    });
    return userMount;
  }


  public render() {
    return (
      <>
        <ToastContainer />

        <Buttons
          login={this.login}
          loginRib={this.loginRib}
          logout={this.logout}
          renewToken={this.renewToken}
          getUser={this.getUser}
          callApi={this.callApi}
          // fetchTest={this.fetchTest}
          // franceConnectSearch={this.franceConnectSearch}
        />

        <AuthContent api={this.state.api} user={this.state.user} />
      </>
    );
  }
}
