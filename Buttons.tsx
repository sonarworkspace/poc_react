import * as React from 'react';

interface IButtonsProps {
  login: () => void;
  getUser:() => void;
  callApi: () => void;
  renewToken: () => void;
  logout: () => void;
  // franceConnectSearch: () => void;
}

const Buttons: React.SFC<IButtonsProps> = props => {
  if (sessionStorage.getItem("oidc.user:https://uneo-dev.cloud.ilex.fr:8443/IdPOAuth2/ids/:306C52206BD41D59")  == null){
    return (
      <div className="row">
      <div className="col-md-12 text-center" style={{ marginTop: '30px' }}>
        <button className="btn btn-primary btn-login" style={{ margin: '10px' }} onClick={props.login}>
          Login
        </button>
        </div>
    </div>
    )
  }
  return (
    <div className="row">
      <div className="col-md-12 text-center" style={{ marginTop: '30px' }}>
        {/* <button className="btn btn-primary btn-login" style={{ margin: '10px' }} onClick={props.login}>
          Login
        </button> */}
        {/* <button className="btn btn-secondary btn-getuser" style={{ margin: '10px' }} onClick={props.getUser}>
          Get User info
        </button> */}
        {/* <button className="btn btn-warning btn-getapi" style={{ margin: '10px' }} onClick={props.callApi}>
          Call API
        </button>
        <button className="btn btn-success btn-renewtoken" style={{ margin: '10px' }} onClick={props.renewToken}>
          Renew Token
        </button> */}

        {/* <button className="btn btn-dark btn-logout" style={{ margin: '10px' }} onClick={props.logout}>
          Logout
        </button> */}
        <a href="https://uneo-dev.cloud.ilex.fr:8443/user/auth/chooseschema?schema=franceconnect&sourceURL=http://my.testdomain.fr:4200/react">
          <button className="btn btn-success btn-getuser" style={{ margin: '10px' }}> Associer compte France connect</button>
        </a>
        {/* <button className="btn btn-danger btn-getuser" style={{ margin: '10px' }} onClick={props.franceConnectSearch}>
          Dissocier compte France connect
          </button> */}
          <a href="https://uneo-dev.cloud.ilex.fr:8443/user/auth/chooseschema?schema=modifyPassword&sourceURL=http://my.testdomain.fr:4200/react&theme=uneo-authentication">
          <button className="btn btn-dark btn-logout" style={{ margin: '10px' }}>Modifier mot de passe</button>
        </a>
       
        <button className="btn btn-primary btn-login" style={{ margin: '10px' }} onClick={props.login}>
          Modifier mon RIB
        </button>

		<a href="https://dev-id.groupe-uneo.fr/user/auth/logout?sourceURL=http://localhost:4200">
          <button className="btn btn-dark btn-logout" style={{ margin: '10px' }} onClick={props.logout}>Logout</button>
        </a>
      </div>
    </div>
  );
};

export default Buttons;
